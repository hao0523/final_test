﻿using final_test.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace final_test.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AboutMe()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Day()
        {
            return View(new DayData());
        }

        [HttpPost]
        public ActionResult Day(DayData data)
        {
            int day = data.Day;
            string level = "";
            if (day == 1)
            {
                level = "星期一";
            }
            else if (day == 2)
            {
                level = "星期二";
            }
            else if (day == 3)
            {
                level = "星期三";
            }
            else if (day == 4)
            {
                level = "星期四";
            }
            else if (day == 5)
            {
                level = "星期五";
            }
            else if (day == 6)
            {
                level = "星期六";
            }
            else if (day == 7)
            {
                level = "星期日";
            }
            else
            {
                level = "輸入錯誤";
            }
            data.Day = day;
            data.Level = level;

            return View(data);
        }
    }
}